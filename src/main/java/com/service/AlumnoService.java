package com.service;

import com.entity.User;
import com.pojo.FormularioFaltaAsistencia;

public interface AlumnoService {

	User findById(Long id);

	void crearFalta(FormularioFaltaAsistencia falta);

}
