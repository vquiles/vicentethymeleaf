package com.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.entity.User;
import com.service.ColegioService;
import com.service.UserService;

@Controller
public class ViewController {

	@Autowired
	private UserService userService;
	@Autowired
	private ColegioService colegioService;

	@GetMapping("/enviar")
	public String enviar() {
		this.colegioService.enviar();
		return "add-falta";
	}

	@GetMapping("/listar")
	public String showSignUpForm(Model model) {
		model.addAttribute("users", this.userService.getAll());
		return "listaUsuarios";
	}

	@PostMapping("/adduser")
	public String addUser(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-user";
		}

		this.userService.save(user);
		model.addAttribute("users", this.userService.getAll());
		return "index";
	}

	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		User user = this.userService.findById(id);

		model.addAttribute("user", user);
		return "update-user";
	}

	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			user.setId(id);
			return "update-user";
		}

		this.userService.save(user);
		model.addAttribute("users", this.userService.getAll());
		return "index";
	}

	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") long id, Model model) {
		User user = this.userService.findById(id);
		this.userService.delete(user);
		model.addAttribute("users", this.userService.getAll());
		return "index";
	}

	// additional CRUD methods
}