package com.service;

import java.util.List;

import com.entity.User;

public interface UserService {

	User findById(Long id);

	User save(User user);

	void delete(User user);

	List<User> getAll();

	User saveOrUpdate(User parametrizacionPrograma);
}
