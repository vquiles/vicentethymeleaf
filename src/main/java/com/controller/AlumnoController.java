package com.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.pojo.FormularioFaltaAsistencia;
import com.service.AlumnoService;

@Controller
public class AlumnoController {

	@Autowired
	private AlumnoService alumnoService;

	@GetMapping("/formularioFaltas")
	public String formularioFaltas(Model model) {
		model.addAttribute("falta", new FormularioFaltaAsistencia());
		return "add-falta";
	}

	@PostMapping("/addfalta")
	public String formularioFaltas(@Valid FormularioFaltaAsistencia falta, BindingResult result, Model model) {
		this.alumnoService.crearFalta(falta);
		model.addAttribute("falta", new FormularioFaltaAsistencia());
		return "add-falta";
	}
}
