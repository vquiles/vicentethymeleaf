package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Colegio;
import com.repository.ColegioRepository;
import com.tasks.GestorCorreo;

@Service
public class ColegioService {

	@Autowired
	private ColegioRepository colegioRepository;

	@Autowired
	private GestorCorreo gestor;

	public void crearColegio(Colegio colegio) {
		this.colegioRepository.save(colegio);
	}

	public List<Colegio> getAllColegios() {
		return this.colegioRepository.findAll();
	}

	public void enviar() {
		this.gestor.sendSimpleMessage("vicenteqs@gmail.com", "asunto", "contenido");
	}

}
