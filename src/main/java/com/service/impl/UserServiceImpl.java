package com.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.User;
import com.repository.UserRepository;
import com.service.UserService;

import exception.NotFoundException;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepository userRepository;

	@Override
	public User findById(final Long id) {
		LOGGER.debug("Getting User with id {}", id);		
		Optional<User> user = userRepository.findById(id);
		if (!user.isPresent()) {
			LOGGER.error("User with id {} not found", id);
			throw new NotFoundException("User con id " + id + " no encontrada");
		}
		return user.get();
	}

	@Override
	@Transactional(readOnly = false)
	public User saveOrUpdate(User user) {
		if (user.getId() != null) {
			LOGGER.debug("User {} with id {}", user, user.getId());
			this.findById(user.getId());
		} else {
			LOGGER.debug("Saving new User {}", user);
		}
		return this.userRepository.save(user);
	}

	@Override
	public User save(User user) {
		return this.userRepository.save(user);
	}

	@Override
	public List<User> getAll() {
		return (List<User>) this.userRepository.findAll();
	}

	@Override
	public void delete(User user) {
		this.userRepository.delete(user);
	}

}
