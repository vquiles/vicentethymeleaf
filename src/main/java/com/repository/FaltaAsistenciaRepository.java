package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.FaltaAsistencia;

@Repository
public interface FaltaAsistenciaRepository extends JpaRepository<FaltaAsistencia, Long> {

}
